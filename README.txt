FlipClock
==============================

This module provides a block implementing FlipClock.

Installation.
=============

1. Unzip the files to the "sites/all/modules" directory and enable the module.

2. Go to the admin/structure/block page to enable and configure the FlipClock block.

Credit
======

The module utilises the FlipClock script written by Objective HTML, LCC (Justin Kimbrell):
http://flipclockjs.com/